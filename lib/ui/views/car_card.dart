import 'package:cars/model/car_model.dart';
import 'package:cars/shared/custom_colors.dart';
import 'package:cars/shared/default_padding_util.dart';
import 'package:cars/shared/font_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CarCard extends StatelessWidget {
  Car value;
  OnCarChangedListener valueChanged;

  CarCard(this.value, this.valueChanged);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Center(
            child: Container(
                margin: EdgeInsets.all(midPadding),
                height: 210,
                decoration: BoxDecoration(
                    color: Color(0xffffffff),
                    border: Border.all(color: Color(0xffeeeeee), width: 1),
                    borderRadius: BorderRadius.circular(1)),
                child: Stack(children: [
                  Align(alignment: Alignment.topCenter, child: createCard()),
                  Align(alignment: Alignment.bottomCenter, child: createView())
                ]))),
        onTap: () {
         // valueChanged.onCarValueChanged(value);
        });
  }

  Widget createCard() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[createGridCard(this.value), createCardBanner()],
    );
  }

  Widget createCardBorder() {
    return Container(
      width: 143,
      height: 210,
      child: createCard(),
    );
  }

  Widget createCardBanner() {
    return Padding(
        padding: EdgeInsets.all(midPadding),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Align(
                alignment: Alignment.centerLeft,
                child: Text(this.value?.title ?? '',
                    maxLines: 1,
                    style: minTextStyle(CustomColors.secondaryGrey))),
            Align(
                alignment: Alignment.centerLeft,
                child: Text(this.value?.mileage ?? '',
                    maxLines: 2,
                    style: TextStyle(
                      fontFamily: 'SFProText',
                      color: Color(0xff333333),
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    ))),
          ],
        ));
  }

  Widget createGridCard(final Car item) {
    return Container(
        width: 143,
        height: 107,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                  width: 143,
                  height: 107,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/car_place_holder.png'),
                      fit: BoxFit.fill,
                    ),
                  )),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                width: 40,
                height: 40,
              ),
            ),
          ],
        ));
  }

  createView() {
    return Container(
        height: 18,
        margin: EdgeInsets.all(midPadding),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
                flex: 7,
                child: Text(
                   value.created_at
                        ,
                    style: TextStyle(
                      fontFamily: 'SFProText',
                      color: Color(0xff999999),
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    ))),
            Expanded(
                flex: 2,
                child: Align(
                    alignment: Alignment.centerRight, child: Text(value.year.toString())))
          ],
        ));
  }
}

abstract class OnCarChangedListener {
  void onCarValueChanged(Car car);
}
