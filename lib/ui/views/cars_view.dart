
import 'package:cars/model/car_model.dart';
import 'package:cars/shared/date_util.dart';
import 'package:cars/shared/default_padding_util.dart';
import 'package:cars/viewmodel/car_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'base_widget.dart';
import 'car_card.dart';

class CarsViiew extends BaseWidget implements OnCarChangedListener {
  CarViewModel _carViewModel;

  @override
  Widget buildChild(BuildContext context) {
    _carViewModel = Provider.of<CarViewModel>(context, listen: false);
    _carViewModel.getAllCars();
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(100.0),
            child: AppBar(
                automaticallyImplyLeading: false, // hides leading widget
                flexibleSpace: Consumer<CarViewModel>(
                  builder: (context, cart, child) =>
                      Container()
//                      createHeaderList(cart.categories),
                  // Build the expensive widget here.
                ))),
        body: Consumer<CarViewModel>(
          builder: (context, cart, child) => GridView.count(
            crossAxisCount: 2,
            shrinkWrap: true,
            padding: EdgeInsets.only(
                left: midPadding, right: midPadding, bottom: midPadding),
            children: cart.cars.map((item)=> CarCard(item,this)).toList())

          ),
          // Build the expensive widget here.
        );

  }





  @override
  void onCarValueChanged(Car car) {
    // TODO: implement onCarValueChanged
  }


}
