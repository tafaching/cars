import 'package:dio/dio.dart';

import 'headers_interceptor.dart';

class DioClient {
  static Dio getDioClient() {
    return Dio()
      ..options =
          BaseOptions(baseUrl: 'http://10.0.2.2:8888/files/api/')
      ..interceptors.add(HeadersInterceptor())
      ..interceptors.add(LogInterceptor(responseBody: true, requestBody: true));
  }
}
