import 'package:cars/model/car_model.dart';
import 'package:cars/model/category.dart';
import 'package:cars/repository/generic_repository.dart';
import 'package:cars/viewmodel/base_view_model.dart';

class CarViewModel extends BaseViewModel with AbstractCarViewModel {
  String endPoint = 'listCars';
  String categoryEndPoint = 'v1/cars/{propertId}';
  String endAllPoint = 'all';
  Car car = Car();
  List<Car> cars = [];
  List<Category> categories = [];
  List<Category> activeCategories = [];

  @override
  void getAllCars() async {
    await GenericRepository<Car>().getAll(endPoint, Car()).then((response) {
      cars = response;
      notifyListeners();
    }).catchError((onError) {
      print('Error ocurred' + onError);
    });

  }

  @override
  void getCarById(String id) async {
    await GenericRepository<Car>()
        .getSingle(endPoint, Car(), queryParams: {'id': id}).then((response) {
      car = response;
    });
    notifyListeners();
  }

  @override
  bool cacheInterval() {
    return true;
  }

  @override
  void getAllCategories() async {
    await GenericRepository<Category>()
        .getAll(endPoint, Category())
        .then((response) {
      categories = response;
    });
    notifyListeners();
  }

  @override
  Future updateFilter(Category value) async {
    if (value.active) {
      activeCategories.removeWhere((item) => item.id == value.id);
      activeCategories.add(value);
    } else {
      activeCategories.removeWhere((item) => item.id == value.id);
    }
    notifyListeners();
  }
}

abstract class AbstractCarViewModel {
  void getAllCars();

  void getAllCategories();

  void getCarById(String id);

  void updateFilter(Category value);
}
