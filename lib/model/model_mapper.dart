abstract class ModelMapper {
  ModelMapper fromJson(Map<String, dynamic> map);

  Map toJson();

  ModelMapper newInstance();
}
