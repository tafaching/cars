import 'package:cars/model/model_mapper.dart';

class Car with ModelMapper {
  int admin_status;
  int body_type_id;
  int brand_id;
  int brand_model_id;
  int category_id;
  int condtion_id;
  String created_at;
  String currency_code;
  String currency_symbol;
  String description;
  int featured;
  String featured_image;
  int fuel_type_id;
  int id;
  String label;
  String mileage;
  int negotiable;
  String regular_price;
  String sale_price;
  String search_price;
  int status;
  String title;
  String top_speed;
  int transmission_type_id;
  String updated_at;
  int user_id;
  String value;
  int views;
  int year;

  Car(
      {this.admin_status,
      this.body_type_id,
      this.brand_id,
      this.brand_model_id,
      this.category_id,
      this.condtion_id,
      this.created_at,
      this.currency_code,
      this.currency_symbol,
      this.description,
      this.featured,
      this.featured_image,
      this.fuel_type_id,
      this.id,
      this.label,
      this.mileage,
      this.negotiable,
      this.regular_price,
      this.sale_price,
      this.search_price,
      this.status,
      this.title,
      this.top_speed,
      this.transmission_type_id,
      this.updated_at,
      this.user_id,
      this.value,
      this.views,
      this.year});

  Car fromJson(Map<String, dynamic> json) {
    return Car(
      admin_status: json['admin_status'],
      body_type_id: json['body_type_id'],
      brand_id: json['brand_id'],
      brand_model_id: json['brand_model_id'],
      category_id: json['category_id'],
      condtion_id: json['condtion_id'],
      created_at: json['created_at'],
      currency_code: json['currency_code'],
      currency_symbol: json['currency_symbol'],
      description: json['description'],
      featured: json['featured'],
      featured_image: json['featured_image'],
      fuel_type_id: json['fuel_type_id'],
      id: json['id'],
      label: json['label'],
      mileage: json['mileage'],
      negotiable: json['negotiable'],
      regular_price: json['regular_price'],
      sale_price: json['sale_price'],
      search_price: json['search_price'],
      status: json['status'],
      title: json['title'],
      top_speed: json['top_speed'],
      transmission_type_id: json['transmission_type_id'],
      updated_at: json['updated_at'],
      user_id: json['user_id'],
      value: json['value'],
      views: json['views'],
      year: json['year'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['admin_status'] = this.admin_status;
    data['body_type_id'] = this.body_type_id;
    data['brand_id'] = this.brand_id;
    data['brand_model_id'] = this.brand_model_id;
    data['category_id'] = this.category_id;
    data['condtion_id'] = this.condtion_id;
    data['created_at'] = this.created_at;
    data['currency_code'] = this.currency_code;
    data['currency_symbol'] = this.currency_symbol;
    data['description'] = this.description;
    data['featured'] = this.featured;
    data['featured_image'] = this.featured_image;
    data['fuel_type_id'] = this.fuel_type_id;
    data['id'] = this.id;
    data['label'] = this.label;
    data['mileage'] = this.mileage;
    data['negotiable'] = this.negotiable;
    data['regular_price'] = this.regular_price;
    data['sale_price'] = this.sale_price;
    data['search_price'] = this.search_price;
    data['status'] = this.status;
    data['title'] = this.title;
    data['top_speed'] = this.top_speed;
    data['transmission_type_id'] = this.transmission_type_id;
    data['updated_at'] = this.updated_at;
    data['user_id'] = this.user_id;
    data['value'] = this.value;
    data['views'] = this.views;
    data['year'] = this.year;
    return data;
  }

  @override
  ModelMapper newInstance() {
    return Car();
  }
}
