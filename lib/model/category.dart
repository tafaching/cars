import 'package:cars/model/model_mapper.dart';

class Category with ModelMapper {
  String id;
  bool active;
  String description;
  String image;

  @override
  ModelMapper fromJson(map) {
    return null;
  }

  @override
  ModelMapper newInstance() {
    return null;
  }

  @override
  Map toJson() {
    return null;
  }
}
