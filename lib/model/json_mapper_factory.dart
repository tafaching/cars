import 'package:cars/model/model_mapper.dart';

class JsonMapperFactory<T extends ModelMapper> {
  List<T> mapToList(T classType, dynamic response) {
    List<T> responseList = [];
    var json = response as List;
    for (var item in json) {
      var map = item as Map;
      responseList.add(classType.newInstance().fromJson(map));
    }
    return responseList;
  }
}
