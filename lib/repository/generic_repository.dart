import 'package:cars/model/json_mapper_factory.dart';
import 'package:cars/model/model_mapper.dart';
import 'package:cars/remote/http_remote_service.dart';

class GenericRepository<T extends ModelMapper> {
  Future<List<T>> getAll(String endPoint, T responseType,
      {Map queryParams, Map pathVariables}) {
    return getData(buildUrl(endPoint, pathVariables), params: queryParams).then(
        (response) => JsonMapperFactory<T>().mapToList(responseType, response));
  }

  Future<T> getSingle(String endPoint, T responseType,
      {Map queryParams, Map pathVariables}) {
    return getData(buildUrl(endPoint, pathVariables))
        .then((response) => responseType.fromJson(response))
        .catchError((onError) {});
  }

  Future post(String endPoint, T request, Map queryParams, Map pathVariables) {
    return postData(buildUrl(endPoint, pathVariables), request.toJson())
        .then((response) {
      return response;
      //Deserialize response
    }).catchError((onError) {
      //do something
    });
  }

  buildUrl(String endPoint, Map pathVariables) {
    if (pathVariables != null) {
      for (MapEntry mapEntry in pathVariables.entries) {
        endPoint = endPoint.replaceAll(
            '{' + mapEntry.key + '}', mapEntry.value.toString());
      }
    }
    return endPoint;
  }
}
