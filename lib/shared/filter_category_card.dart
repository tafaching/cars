import 'package:cached_network_image/cached_network_image.dart';
import 'package:cars/model/category.dart';
import 'package:cars/shared/round_check_box.dart';
import 'package:cars/shared/single_shimmer_loader.dart';
import 'package:flutter/material.dart';

import 'custom_colors.dart';
import 'default_padding_util.dart';

// ignore: must_be_immutable
class FilterCardWidget extends StatelessWidget {
  Category value;
  OnCategoryChangedListener valueChanged;

  FilterCardWidget({@required this.value, @required this.valueChanged});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: createGridCard(value),
      onTap: () {
        value.active = !value.active;
        valueChanged.onValueChanged(value);
      },
    );
  }

  Widget createGridCard(final Category item) {
    return Column(children: <Widget>[
      Expanded(
          flex: 1,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      color: CustomColors.primaryWhite,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                            color: CustomColors.primaryGray,
                            offset: Offset(1, 2),
                            blurRadius: 9,
                            spreadRadius: 0)
                      ],
                    )),
              ),
              Center(
                child: Container(
                  width: 44,
                  height: 52,
                  child: CachedNetworkImage(
                      imageUrl: value.image,
                      placeholder: (context, url) =>
                          SingleShimmerLoader(buildImagePlaceholder()),
                      imageBuilder: (context, imageProvider) {
                        return Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.contain,
                            ),
                          ),
                        );
                      }),
                ),
              ),
              Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: smallPadding, right: smallPadding),
                    child: RoundCheckBox(
                      value: item.active ?? false,
                      activeColor: CustomColors.primaryBlue,
                      checkColor: CustomColors.primaryWhite,
                      onChanged: (isCheck) {},
                    ),
                  )),
            ],
          )),
      Center(
        child: Padding(
            padding: EdgeInsets.only(top: midPadding),
            child: Text(item.description ?? '',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                softWrap: false,
                style: TextStyle(
                  color: Color(0xff333333),
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ))),
      )
    ]);
  }

  buildImagePlaceholder() {
    return Container(
      width: 44,
      height: 52,
      color: Colors.white,
    );
  }
}

abstract class OnCategoryChangedListener {
  void onValueChanged(Category value);
}
