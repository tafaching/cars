import 'package:cars/shared/custom_colors.dart';
import 'package:flutter/material.dart';

class RoundCheckBox extends StatelessWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final Color activeColor;
  final Color checkColor;
  final bool tristate;
  final MaterialTapTargetSize materialTapTargetSize;

  RoundCheckBox({
    Key key,
    @required this.value,
    this.tristate = false,
    @required this.onChanged,
    this.activeColor,
    this.checkColor,
    this.materialTapTargetSize,
  })  : assert(tristate != null),
        assert(tristate || value != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return value
        ? ClipOval(
            child: SizedBox(
              width: Checkbox.width,
              height: Checkbox.width,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                      width: 2,
                      color: Theme.of(context).unselectedWidgetColor ??
                          CustomColors.primaryWhite),
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Checkbox(
                  value: value,
                  tristate: tristate,
                  onChanged: onChanged,
                  activeColor: activeColor,
                  checkColor: checkColor,
                  materialTapTargetSize: materialTapTargetSize,
                ),
              ),
            ),
          )
        : Container();
  }
}
