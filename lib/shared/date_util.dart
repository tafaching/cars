import 'package:intl/intl.dart';

String getStartDate(date) {
  var tempStart = date.split("T");
  return tempStart[0];
}

String getEndDate(date) {
  var tempEnd = date.split("T");
  return tempEnd[0];
}

DateTime getDateByString(date) {
  var parsedDate = DateTime.parse(date);
  return parsedDate;
}

String getDateByFormat(DateTime date, String format) {
  if (date == null) return null;
  var formattedDate = new DateFormat(format).format(date);
  return '$formattedDate';
}

String formatDate(String date, String format) {
  var strDate = date.split("T");
  var parsedDate = DateTime.parse(strDate[0]);
  var formattedDate = new DateFormat(format).format(parsedDate);

  return '$formattedDate';
}

String formatTime(String date) {
  var strDate = date.split("T");
  var time = strDate[1].substring(0, 5);
  return time;
}

String formatTimeFromDate(DateTime time) {
  var formattedDate = DateFormat('kk:mm').format(time);
  // var time = time.toString();
  // var time = strDate[1].substring(0, 5);
  return formattedDate;
}

String dateRange;
String getFormattedDate(startDate, endDate) {
  var tempEndDate = DateTime.parse(getEndDate(endDate));
  var tempStartDate = DateTime.parse(getStartDate(startDate));
  var tempStartMonth =  DateFormat('MMMM yyyy').format(tempStartDate);
  var tempEndMonth =  DateFormat('MMMM yyyy').format(tempEndDate);

  if (tempEndDate == tempStartDate) {
    dateRange = "${ DateFormat('dd MMMM yyyy').format(tempStartDate)}";
  } else {
    if (tempStartMonth == tempEndMonth) {
      endDate =  DateFormat('dd MMMM yyyy').format(tempEndDate);
      startDate =  DateFormat('dd').format(tempStartDate);
      dateRange = '${startDate} - ${endDate}';
    } else {
      endDate =  DateFormat('dd MMMM yyyy').format(tempEndDate);
      startDate =  DateFormat('dd MMMM').format(tempStartDate);
      dateRange = '${startDate} - ${endDate}';
    }
  }
  return dateRange;
}
