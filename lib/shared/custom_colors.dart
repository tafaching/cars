import 'dart:ui';

class CustomColors {
  CustomColors({bool dark = false});

  static get secondaryGrey {
    return Color(0xff999999);
  }

  static get primaryBlue {
    return Color.fromRGBO(9, 68, 148, 1);
  }

  static get primaryWhite {
    return Color.fromRGBO(255, 255, 255, 1);
  }

  static get primaryGray {
    return Color.fromRGBO(144, 143, 144, 1);
  }

  static get black {
    return Color.fromRGBO(51, 51, 51, 1);
  }
}
