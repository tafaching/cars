import 'package:flutter/material.dart';

minTextStyle(Color color) => TextStyle(
      fontFamily: 'SFProText',
      color: color,
      fontSize: 10,
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.normal,
      letterSpacing: 0,
    );

normalTextStyle(Color color) => TextStyle(
      fontFamily: 'SFProText',
      color: color,
      fontSize: 12,
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.normal,
      letterSpacing: 0,
    );

maxTextStyle(Color color) => TextStyle(
      fontFamily: 'SFProText',
      color: color,
      fontSize: 15,
      fontWeight: FontWeight.w700,
      fontStyle: FontStyle.normal,
      letterSpacing: 0,
    );
