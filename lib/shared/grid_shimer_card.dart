import 'package:cars/shared/round_check_box.dart';
import 'package:cars/shared/single_shimmer_loader.dart';
import 'package:flutter/material.dart';

import 'custom_colors.dart';
import 'default_padding_util.dart';

class GridCardShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return createGridCard();
  }

  Widget createGridCard() {
    return Column(children: <Widget>[
      Expanded(
          flex: 1,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Container(
                    width: 100,
                    height: 110,
                    decoration: BoxDecoration(
                      color: Color(0x98dcdcdc),
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0x98dcdcdc),
                            offset: Offset(1, 2),
                            blurRadius: 9,
                            spreadRadius: 0)
                      ],
                    )),
              ),
              Center(
                child: Container(
                  width: 44,
                  height: 52,
                  child: SingleShimmerLoader(buildImagePlaceholder()),
                ),
              ),
              Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: smallPadding, right: smallPadding),
                    child: RoundCheckBox(
                      value: false,
                      activeColor: CustomColors.primaryBlue,
                      checkColor: CustomColors.primaryWhite,
                      onChanged: (isCheck) {},
                    ),
                  )),
            ],
          )),
      Center(
        child: Padding(
            padding: EdgeInsets.only(top: midPadding),
            child: SingleShimmerLoader(buildTextPlaceholder())),
      )
    ]);
  }

  List<Widget> createGridShimmerList() {
    return [
      this,
      this,
      this,
      this,
      this,
      this,
      this,
      this,
      this,
    ];
  }

  buildImagePlaceholder() {
    return Container(
      width: 44,
      height: 52,
      color: Colors.white,
    );
  }

  buildTextPlaceholder() {
    return Container(
      height: 20,
      color: Colors.white,
    );
  }
}
